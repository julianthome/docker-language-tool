# Source: https://github.com/silvio/docker-languagetool

FROM openjdk:14-alpine

MAINTAINER Julian Thome <jthome@gitlab.com>

RUN apk add --no-cache libgomp gcompat libstdc++ bash

ENV VERSION 5.0
RUN wget https://www.languagetool.org/download/LanguageTool-$VERSION.zip && \
    unzip LanguageTool-$VERSION.zip && \
    rm LanguageTool-$VERSION.zip

WORKDIR /LanguageTool-$VERSION

